package Controlador;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import Vista.dlgVista;
import Modelo.dbCotizacion;
import Modelo.Cotizacion;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controlador implements ActionListener {

    private dbCotizacion db;
    private Cotizacion cot;
    private dlgVista vista;
    private boolean isInsertar = false;
    //inicializacion de botones

    public Controlador(dbCotizacion db, Cotizacion cot, dlgVista vista) {
        this.db = db;
        this.cot = cot;
        this.vista = vista;

        vista.rdbtn1.addActionListener(this);
        vista.rdbtn2.addActionListener(this);
        vista.rdbtn3.addActionListener(this);
        vista.rdbtn4.addActionListener(this);
        vista.rdbtn5.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnBorrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);

    }

    private void iniciarvista() throws Exception {
        vista.jTbl1.setModel(db.listar());
        vista.setTitle(";;COTIZACIONES;;");
        vista.setSize(650, 650);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            this.habilitar();
        }
        
        if (e.getSource() == vista.btnGuardar) {
            if(!ValiEmpty()){
                JOptionPane.showMessageDialog(vista, "No deje ningun espacio en vacio");
            }
            else{
                try {
                    cot = (Cotizacion)db.buscar(vista.txtNumCotizaciones.getText());
                    
                    
                        if (this.isInsertar != true) {
                            if(!cot.getNumCotizacion().equals("")){
                                JOptionPane.showMessageDialog(vista,"Ese codigo ya existe");
                            }
                            else{
                                cot.setNumCotizacion(vista.txtNumCotizaciones.getText());
                                cot.setDescripcionAutomovil(vista.txtDescAut.getText());
                                cot.setPrecioAutomovil(Integer.parseInt(vista.txtPrecio.getText()));
                                cot.setPorcentajePago(Integer.parseInt(vista.spnPagoIni.getValue().toString()));
                                plazoG();
                                db.insertar(cot);
                                JOptionPane.showMessageDialog(vista, "se guardo correctamente");
                            } 
                        }
                        else{
                            cot.setNumCotizacion(vista.txtNumCotizaciones.getText());
                            cot.setDescripcionAutomovil(vista.txtDescAut.getText());
                            cot.setPrecioAutomovil(Integer.parseInt(vista.txtPrecio.getText()));
                            cot.setPorcentajePago(Integer.parseInt(vista.spnPagoIni.getValue().toString()));
                            db.actualizar(cot);
                            vista.jTbl1.setModel(db.listar());
                        }
                        vista.jTbl1.setModel(db.listar());
                        limpiar();
                        this.desahabilitar();
                    
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista,"Surgio un error al Guardar o Actualizar"+ex.getMessage());
                }
            }   
        }
        
        else if (e.getSource() == vista.btnBuscar) {
            if("".equals(vista.txtNumCotizaciones.getText())){
                JOptionPane.showMessageDialog(vista, "Porfavor ingrese un numero de cotizacion antes de buscar");
            }
            else{
                try {
                    cot = (Cotizacion) db.buscar(vista.txtNumCotizaciones.getText());
                    if(cot.getDescripcionAutomibil().equals("")){
                        JOptionPane.showMessageDialog(vista, "El numero que ingreso no existe");
                    }
                    else{
                        vista.txtDescAut.setText(cot.getDescripcionAutomibil());
                        vista.txtPrecio.setText(String.valueOf(cot.getPrecioAutomovil()));
                        vista.spnPagoIni.setValue(cot.getPorcentajePago());
                        vista.txtPagoini.setText(String.valueOf(cot.calcularPagoInicial()));
                        vista.txtTotalFini.setText(String.valueOf(cot.calcularTotalAfinanciar()));
                        vista.txtPagoMens.setText(String.valueOf((int) cot.calcularPagoMensual()));
                        plazoB();
                        isInsertar=true;
                    }
                } catch (Exception ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
 
        }
        
        else if (e.getSource() == vista.btnBorrar) {
            if("".equals(vista.txtNumCotizaciones.getText())){
                JOptionPane.showMessageDialog(vista, "Porfavor ingrese un numero de cotizacion antes de borrar");
            }
            else{
                try {
                    cot = (Cotizacion) db.buscar(vista.txtNumCotizaciones.getText());
                    if("".equals(cot.getDescripcionAutomibil())){
                        JOptionPane.showMessageDialog(vista, "El numero que ingreso no existe");
                    }
                    else{
                        db.borrar(vista.txtNumCotizaciones.getText());
                        vista.jTbl1.setModel(db.listar());
                        JOptionPane.showMessageDialog(vista, "se borro correctamente");
                    }
                } catch (Exception ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }
        
        else if (e.getSource() == vista.btnLimpiar) {
            this.limpiar();
        }
        
        else if (e.getSource() == vista.btnCancelar) {
            this.limpiar();
            this.desahabilitar();
        }
        
        else if (e.getSource() == vista.btnCerrar) {
            if (JOptionPane.showConfirmDialog(vista, "Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            } else {
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }
        }
    }

    //metodos de libreria
    public void habilitar() {
        vista.txtNumCotizaciones.setEnabled(true);
        vista.txtDescAut.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.spnPagoIni.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnBorrar.setEnabled(true);
        vista.rdbtn1.setEnabled(true);
        vista.rdbtn2.setEnabled(true);
        vista.rdbtn3.setEnabled(true);
        vista.rdbtn4.setEnabled(true);
        vista.rdbtn5.setEnabled(true);

    }

    public void desahabilitar() {
        vista.txtNumCotizaciones.setEnabled(false);
        vista.txtDescAut.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.spnPagoIni.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnBorrar.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnBorrar.setEnabled(false);
        vista.btnLimpiar.setEnabled(false);
        vista.btnCancelar.setEnabled(false);
        vista.rdbtn1.setEnabled(false);
        vista.rdbtn2.setEnabled(false);
        vista.rdbtn3.setEnabled(false);
        vista.rdbtn4.setEnabled(false);
        vista.rdbtn5.setEnabled(false);
    }

    public void limpiar() {
        vista.txtNumCotizaciones.setText("");
        vista.txtDescAut.setText("");
        vista.txtPrecio.setText("");
        vista.spnPagoIni.setValue(0);
        vista.GrupoPlazos.clearSelection();
        vista.txtPagoini.setText("");
        vista.txtPagoMens.setText("");
        vista.txtTotalFini.setText("");
        vista.txtPagoMens.setText("");
        vista.txtPagoini.setText("");
        vista.txtTotalFini.setText("");
    }
    
    public boolean ValiEmpty(){
        int sp =(int) vista.spnPagoIni.getValue();
        if(!vista.txtNumCotizaciones.getText().isEmpty() 
                && !vista.txtDescAut.getText().isEmpty()
                && !vista.txtPrecio.getText().isEmpty()
                && vista.GrupoPlazos.getSelection()!=null
                && sp !=0){
            return true;
        }
        else{
            return false;
        }
        
    }
    
    public void plazoG(){
        if (vista.rdbtn1.isSelected()) {
            cot.setPlazo(12);
        }
        else if (vista.rdbtn2.isSelected()) {
            cot.setPlazo(24);
        }
        else if (vista.rdbtn3.isSelected()) {
            cot.setPlazo(36);
        }
        else if (vista.rdbtn4.isSelected()) {
            cot.setPlazo(48);
        }
        else if (vista.rdbtn5.isSelected()) {
            cot.setPlazo(60);
        }
    }
    
    public void plazoB(){
        switch (cot.getPlazo()) {
            case 12:
                vista.rdbtn1.setSelected(true);
                break;
            case 24:
                vista.rdbtn2.setSelected(true);
                break;
            case 36:
                vista.rdbtn3.setSelected(true);
                break;
            case 48:
                vista.rdbtn4.setSelected(true);
                break;
            case 60:
                vista.rdbtn5.setSelected(true);
                break;
        }
    }

    public static void main(String[] args) throws Exception {
        dbCotizacion db = new dbCotizacion();
        Cotizacion cot = new Cotizacion();
        dlgVista vista = new dlgVista(new JFrame(), true);
        Controlador con = new Controlador(db, cot, vista);
        con.iniciarvista();

    }
}
