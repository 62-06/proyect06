
package Modelo;
public class Cotizacion {

    private int idCotizacion;
    private String numCotizacion;
    private String descripcionAutomovil;
    private int precioAutomovil;
    private int porcentajePago;
    private int plazo;

    //metodos constructor
    public Cotizacion() {
        this.idCotizacion = 0;
        this.numCotizacion = "";
        this.descripcionAutomovil = "";
        this.precioAutomovil = 0;
        this.porcentajePago = 0;
        this.plazo = 0;
    }

    public Cotizacion(String numCotizacion, String descripcionAutomovil, int precioAutomovil, int porcentajePago, int plazo) {
        this.numCotizacion = numCotizacion;
        this.descripcionAutomovil = descripcionAutomovil;
        this.precioAutomovil = precioAutomovil;
        this.porcentajePago = porcentajePago;
        this.plazo = plazo;
    }

    public Cotizacion(Cotizacion x) {
        this.numCotizacion = x.numCotizacion;
        this.descripcionAutomovil = x.descripcionAutomovil;
        this.precioAutomovil = x.precioAutomovil;
        this.porcentajePago = x.porcentajePago;
        this.plazo = x.plazo;
    }

    // metodos para cambiar o conocer el estado de los objetos
    public void setIdCotizacion(int idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public void setNumCotizacion(String numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }

    public void setPrecioAutomovil(int precioAutomovil) {
        this.precioAutomovil = precioAutomovil;
    }

    public void setPorcentajePago(int porcentajePago) {
        this.porcentajePago = porcentajePago;
    }

    /*public void setPagoInicial(int pagoInicial){
 this.pagoInicial = pagoInicial;
 }*/
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public String getNumCotizacion() {
        return this.numCotizacion;
    }

    public int getIdCotizacion() {
        return idCotizacion;
    }

    public String getDescripcionAutomibil() {
        return this.descripcionAutomovil;
    }

    public int getPrecioAutomovil() {
        return this.precioAutomovil;
    }

    public int getPorcentajePago() {
        return this.porcentajePago;
    }

    public int getPlazo() {
        return this.plazo;
    }

    public float calcularPagoInicial() {
        return this.precioAutomovil * this.porcentajePago / 100;
    }

    public float calcularTotalAfinanciar() {
        return this.precioAutomovil - this.calcularPagoInicial();
    }

    public float calcularPagoMensual() {
        return this.calcularTotalAfinanciar() / this.plazo;
    }

    public void imprimirCotizacion() {
        System.out.println("======= TOYOTA MAZATLAN =======");
        System.out.println(" ");
        System.out.println("numero de cotizacion " + this.numCotizacion);
        System.out.println("Descripcion del Automivil " + this.descripcionAutomovil);
        System.out.println("El costo del Automovil es " + this.precioAutomovil);
        System.out.println("Pago inicial del Automovil es " + this.calcularPagoInicial());
        System.out.println("Total a Financiar es " + this.calcularTotalAfinanciar());
        System.out.println("El Pago mensual es " + this.calcularPagoMensual());
        System.out.println();
    }

}
